#!/usr/bin/env python3

from datetime import date
import json
import os
import re
import yaml

generated_yaml = """\
include:
  - remote: 'https://gitlab.com/deploy2zenodo/deploy2zenodo/-/releases/permalink/latest/downloads/deploy2zenodo.yaml'

stages:
- container-build
- test
- release
- publication

.runner-matrix:
  parallel:
    matrix:
        - TAG: saas-linux-medium-amd64
          ARCH: amd64
        - TAG: saas-linux-medium-arm64
          ARCH: arm64
  tags:
    - $TAG

"""

# Should be made into a jinja template
JOB_TEMPLATE = """\
"{name}_build_job":
  extends: .runner-matrix
  stage: container-build
  image: docker:git
  services:
  - docker:dind
  # Build container for each architecture, skip if already present
  script:
    - echo "{name} {version}"
    - git clone {repo} {name} && cd {name}
    - git checkout {version}
    {build_workdir_command}
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - |
      if ! docker manifest inspect $CI_REGISTRY_IMAGE/{name}:{version}-$ARCH > /dev/null; then
        docker build . -f {dockerfile_location} {build_args} --tag $CI_REGISTRY_IMAGE/{name}:{version}-$ARCH
        docker push $CI_REGISTRY_IMAGE/{name}:{version}-$ARCH
      fi
  needs: {needs}

"{name}_multiarch_push_job":
  stage: container-build
  image: docker:git
  services:
  - docker:20.10.8-dind
  script:
    - echo "Tagging a release candidate version for {name} {version}"
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker manifest create $CI_REGISTRY_IMAGE/{name}:{version}-rc \
      --amend $CI_REGISTRY_IMAGE/{name}:{version}-amd64 \
      --amend $CI_REGISTRY_IMAGE/{name}:{version}-arm64
    - docker manifest push $CI_REGISTRY_IMAGE/{name}:{version}-rc
    - docker manifest inspect $CI_REGISTRY_IMAGE/{name}:{version}-rc
  needs:
    - {name}_build_job

"{name}_test_job":
  extends: .runner-matrix
  stage: test
  image: docker:git
  services:
  - docker:dind
  # Test software for each architecture
  script:
    - echo "{name} {version}"
    - docker pull $CI_REGISTRY_IMAGE/{name}:{version}-rc
    {test_commands}

"{name}_release_job":
  stage: release
  image: docker:git
  services:
  - docker:dind
  # Tag a release version
  script:
    - echo "Tagging Release for {name} {version}"
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker manifest create $CI_REGISTRY_IMAGE/{name}:{version} \
      --amend $CI_REGISTRY_IMAGE/{name}:{version}-amd64 \
      --amend $CI_REGISTRY_IMAGE/{name}:{version}-arm64
    - docker manifest push $CI_REGISTRY_IMAGE/{name}:{version}
    - docker manifest inspect $CI_REGISTRY_IMAGE/{name}:{version}

"{name}_prepare_deploy2zenodo":
  stage: release
  image:
    name: docker:git
  variables:
    DEPLOY2ZENODO_JSON: "{name}_metadata.json"
    DEPLOY2ZENODO_UPLOAD: "{name}v{version}.zip"
    VARIABLE_FILE: "{name}_variables.env"
  script:
    # prepare
    - TAG={version}
    - echo "{zenodo_metadata}" > "$DEPLOY2ZENODO_JSON"
    # prepare release
    - echo "DESCRIPTION=README.md" > $VARIABLE_FILE
    - echo "TAG=$TAG" >> $VARIABLE_FILE
    # prepare deploy2zenodo
    - echo "DEPLOY2ZENODO_JSON=$DEPLOY2ZENODO_JSON" >> $VARIABLE_FILE
    - git clone {repo} {name} && cd {name}
    - git archive --format zip --output ../"$DEPLOY2ZENODO_UPLOAD" "$TAG"
    - echo "DEPLOY2ZENODO_UPLOAD=$DEPLOY2ZENODO_UPLOAD" >> $VARIABLE_FILE
  artifacts:
    reports:
      dotenv: {name}_variables.env
    paths:
      - $DEPLOY2ZENODO_JSON
      - $DEPLOY2ZENODO_UPLOAD


"{name}_deploy2zenodo":
  stage: publication
  image:
    name: docker:git
  before_script:
    - apk add --no-cache curl jq
  extends: .deploy2zenodo
  dependencies:
    - {name}_prepare_deploy2zenodo
  variables:
    DEPLOY2ZENODO_API_URL: "https://sandbox.zenodo.org/api"
    DEPLOY2ZENODO_DEPOSITION_ID: "{zenodo_deposition_id}"
    DEPLOY2ZENODO_JSON: "{name}_metadata.json"
    DEPLOY2ZENODO_UPLOAD: "{name}v{version}.zip"
    VARIABLE_FILE: "{name}_variables.env"
"""

GENERAL_JOBS = """\
"release_cvmfs_job":
  stage: release
  image: python:3.12-slim-bullseye
  # Create DUCC recipe with release images
  script:
    - |
      cat > ducc-recipe.yaml << EOF
      {ducc_wishlist}
      EOF
  artifacts:
    paths:
      - ducc-recipe.yaml

"deploy2zenodo":
  rules: [when: never]
"""


def create_ducc_wishlist(release_images):
    input_list = ""
    for release_image in release_images:
        input_list += f"        - {release_image}\n"

    return f"""version: 1
      user: cvmfsunpacker
      cvmfs_repo: 'unpacked.cern.ch'
      output_format: 'https://gitlab-registry.cern.ch/unpacked/sync/$(image)'
      input:
{input_list}
"""

base_path = f"{os.getcwd()}/software/"
release_images = []

for dir_name in os.listdir(base_path):
    dir_path = f"{base_path}/{dir_name}"
    if dir_name.startswith('.') or not os.path.isdir(dir_path):
        continue  # or use whatever condition makes sense for you
    for filename in os.listdir(dir_path):
        if re.search(".ya?ml$", filename):
            with open(os.path.join(dir_path, filename), 'r') as f:
                values = yaml.safe_load(f)

                name = values["name"]
                version = values["version"]

                build_args = ""
                if "build_args" in values:
                    build_args=" ".join(f"--build-arg {arg}" for arg in values["build_args"])

                build_workdir = ""
                build_workdir_command = ""
                if "build_workdir" in values:
                    build_workdir=f"{values["build_workdir"]}"
                    build_workdir_command=f"- cd {build_workdir}"

                needs = "[]"
                if "dependency" in values:
                    needs = f"[\"{values["dependency"]["package"]}_multiarch_push_job\"]"

                release_image = f"$CI_REGISTRY_IMAGE/{name}:{version}"
                test_image = f"{release_image}-rc"
                test_commands = ""
                if "test_commands" in values:
                  test_commands = "    ".join(
                      [f"- docker run {test_image} {cmd}\n" for cmd in values["test_commands"]]
                    )


                release_images.append(release_image)

                zenodo_metadata = {
                    "metadata":{
                        "creators":[{"name":"user, test"}],
                        "license":{"id":"GPL-3.0-or-later"},
                        "title":name,
                        "version":version,
                        "upload_type":"software",
                        "publication_date":date.today().strftime("%Y-%m-%d"),
                    }
                }

                zenodo_deposition_id = "create NEW record"
                if "zenodo_deposition_id" in values:
                    zenodo_deposition_id = values["zenodo_deposition_id"]



            job_definition = JOB_TEMPLATE.format(
                name=values["name"],
                version=values["version"],
                repo=values["repo"],
                dockerfile_location=values["dockerfile_location"],
                build_args=build_args,
                build_workdir=build_workdir,
                build_workdir_command=build_workdir_command,
                needs = needs,
                test_commands = test_commands,
                zenodo_metadata = json.dumps(zenodo_metadata, separators=(',', ':')).replace('"', '\\"'),
                zenodo_deposition_id=zenodo_deposition_id
            )
            generated_yaml += job_definition

            general_job_definition = GENERAL_JOBS.format(
              # ducc_wishlist=create_ducc_wishlist(release_images)
              ducc_wishlist=""
            )

            generated_yaml += general_job_definition

print(generated_yaml)
