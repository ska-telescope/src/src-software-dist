#!/bin/python3

import collections
import glob
import os
import pathlib
import subprocess
import sys

try:
    print("Looking for Apptainer")
    subprocess.run(["apptainer", "--version"])
except:
    print("Could not find Apptainer. Exiting.")
    exit(1)

OUTPUT_DIR = sys.argv[1]
if not OUTPUT_DIR:
    print("Please specify output dir")
    exit(1)
OUTPUT_DIR = f"{OUTPUT_DIR}/software/linux"
pathlib.Path(OUTPUT_DIR).mkdir(parents=True, exist_ok=True)

# Change working directory to script directory
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

ARCHITECTURES = {
   "aarch64": {},
   "x86_64": {
        "generic": [
            {
                "march": "x86-64",
                "mtune": "generic",
                "path": "x86_64/generic",
            },
        ],
        "amd": [
            {
                "march": "znver2",
                "mtune": "znver2",
                "path": "x86_64/amd/zen2",
            },
            {
                "march": "znver3",
                "mtune": "znver3",
                "path": "x86_64/amd/zen3",
            },
        ],
        "intel": [
            {
                "march": "haswell",
                "mtune": "haswell",
                "path": "x86_64/intel/haswell",
            },
            {
                "march": "skylake",
                "mtune": "skylake",
                "avx512": "false",
                "path": "x86_64/intel/skylake",
            },
        ]
    }
}

print("\nAttempting to build for following architectures and microarchitectures:")
for isa, architectures in ARCHITECTURES.items():
    print(f"{isa}:")
    for arch in architectures:
        print(f"  {arch}:")
        for options in architectures[arch]:
            print(f"    march={options['march']}, mtune={options['mtune']}")


print("\nCollecting recipes from recipes directory")
recipe_directories = [f for f in os.scandir("recipes") if f.is_dir()]

print("Found recipe directories for the following software:")
for name in [directory.name for directory in recipe_directories]:
    print(f" - {name}")


def select_recipes(directory):
    available_isas = [f.name for f in os.scandir(directory.path) if f.is_dir()]
    for isa, architectures in ARCHITECTURES.items():
        if isa not in available_isas:
            print(f"No {isa} directory found for {directory.name}")
            continue

        recipes = glob.glob(f"recipes/{directory.name}/{isa}/**/*.def", recursive=True)
        # Find recipes for specific (micro)architectures
        match_generic_recipe = glob.glob(f"recipes/{directory.name}/{isa}/*.def")
        if not match_generic_recipe:
            match_generic_recipe = glob.glob(f"recipes/{directory.name}/{isa}/generic/*.def")

        if not match_generic_recipe:
            print(f"Failed to find generic {isa} recipe for {directory.name}")
            generic_recipe = None
        else:
            generic_recipe = match_generic_recipe[0]
            print(f"Using {generic_recipe} as a {isa} generic recipe for {directory.name}")

        recipes_with_options = collections.defaultdict(list)

        for architecture, options in architectures.items():
            matching_recipes = [recipe_path for recipe_path in recipes if f"{isa}/{architecture}" in recipe_path]
            if matching_recipes:
                print(f"Found {architecture} recipe for {directory.name}: {matching_recipes[0]}")
                recipes_with_options[(matching_recipes[0], architecture, directory.name)] += options
            elif not matching_recipes and generic_recipe:
                print(f"Failed to find {architecture} recipe for {directory.name}. Using generic.")
                recipes_with_options[(generic_recipe, architecture, directory.name)] += options
            else:
                print(f"Failed to find {architecture} recipe for {directory.name} and no generic recipe. Skipping.")
        return recipes_with_options

def build_recipe(recipe_architecture, options):
    recipe, architecture, software = recipe_architecture
    for option in options:
        path = f"{OUTPUT_DIR}/{option['path']}/{software}.sif"
        if pathlib.Path(path).exists():
            print(f"{path} already exists. Skipping build.")
            continue
        pathlib.Path(path).parents[0].mkdir(parents=True, exist_ok=True)
        command = [
            "apptainer",
            "build",
            "--fakeroot",
            "--warn-unused-build-args",
            "--build-arg",
            f"arch={architecture}",
            "--build-arg",
            f"march={option['march']}",
            "--build-arg",
            f"mtune={option['mtune']}",
            path,
            recipe,
        ]
        print(f"Building recipe: {command}")
        try:
            subprocess.run(command)
        except:
            print(f"Failed to run command: {command}")

for directory in recipe_directories:
    print(f"\nBuilding containers for {directory.name}")
    recipes_with_options = select_recipes(directory)
    for recipe, options in recipes_with_options.items():
        build_recipe(recipe, options)
