# this script is *sourced*, not executed, so can't rely on $0 to determine path to self
# $BASH_SOURCE points to correct path, see also http://mywiki.wooledge.org/BashFAQ/028
EESSI_INIT_DIR_PATH=$(dirname $(realpath $BASH_SOURCE))


all_cpupaths=$(${EESSI_INIT_DIR_PATH}/eessi_archdetect.sh -a cpupath)
echo "FOUND: ${all_cpupaths}"
# iterate over colon-separated list verifying if the architecture is present
#   under $EESSI_PREFIX/software/$EESSI_OS_TYPE; if so use the architecture as best match
IFS=: read -r -a archs <<< "${all_cpupaths}"
for arch in "${archs[@]}"; do
if [ -d ${EESSI_PREFIX}/software/${EESSI_OS_TYPE}/${arch} ]; then
    export EESSI_SOFTWARE_SUBDIR=${arch}
    show_msg "archdetect says ${EESSI_SOFTWARE_SUBDIR}"
    break
fi
done
