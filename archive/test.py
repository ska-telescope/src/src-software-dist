#!/bin/python3

import collections
import glob
import os
import pathlib
import subprocess
import sys

paths = ["/Users/kok00004/dev/surf/ska/src-software-dist/test.sh", "/Users/kok00004/dev/surf/ska/src-software-dist/nofile.txt"]

for path in paths:
    if pathlib.Path(path).is_file():
        print(f"{path} already exists. Skipping build.")
        continue
    print("creating path", path)
