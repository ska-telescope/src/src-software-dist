# DUCC Installation

## Set up Ansible environment

```{bash}
python -m venv venv
source venv/bin/activate
pip install -r requirements
ansible-galaxy install -r ansible/requirements.yaml
```

## Installation via Ansible

Dry-run:

```{bash}
ansible-playbook -i ansible/inventory ansible/playbook.yaml --check --diff --ask-vault-password
```

Actual run:

```{bash}
ansible-playbook -i ansible/inventory ansible/playbook.yaml --diff --ask-vault-password
```
